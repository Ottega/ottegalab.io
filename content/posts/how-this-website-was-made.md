---
title: "How This Website Was Made"
date: 2022-06-08T15:13:53+02:00
draft: true
---

1.  Setup Hugo
    1.  Download Hugo
    2.  Choose & download Hugo Theme
    3.  Test Hugo & Theme
    4.
2.  1.  Setup Domain

3.  Set up Git Repositories

    1.  Theme Respository
        1. Clone theme to local and rename original remote to upstream
        2. add new remote on own git respository called origin
        3. push it to own repository
    2.  Main Blog Repository
        1. Create Gitlab Repository
           1. Clone to local
        2. Add Theme as Submoudle to main repository
           1. Change url in .gitmodules to "../../Ottega/LoveIt.git"
        3. Setup GitLab Pipeline
           1. test: checks if website content is ok
           2. pages: generates static website
           3. build_deploy: builds docker container and pushes to Docker registry
    3.  Dicussions Repository
        1. Create GitHub

4.  Modify Theme
    1.  Add changes to theme
    2.  push changes to origin
        1. `git push main origin`
    3.  update submodule and push to main repo
        1. `git submodule update --remote`
        2. push to main repo
5.  Create Post
    1.  Create Post
    2.  Push to git
    3.  hope it works

```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```
