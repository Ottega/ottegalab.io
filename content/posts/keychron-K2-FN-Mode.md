---
title: "Keychron K2 FN Mode"
date: 2022-06-06T13:19:43+02:00
---

```bash
echo "options hid_apple fnmode=2" | sudo tee /etc/modprobe.d/hid_apple.conf
```

Ubuntu[^main]:
```bash
sudo update-initramfs -u && reboot
```
Fedora[^fedora]:
```bash
sudo dracut --regenerate-all && reboot
```
Arch[^arch]:
```bash
sudo mkinitcpio -P && reboot
```
[^main]: https://gist.github.com/mid9commander/669273
[^fedora]: https://www.reddit.com/r/linux4noobs/comments/rrt6pw/fedora_equivalent_for_updateinitramfs_command/
[^arch]: https://wiki.archlinux.org/title/Mkinitcpio#Manual_generation