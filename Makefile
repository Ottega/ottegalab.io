# Author: TW (https://gitlab.com/Gelaber)
# Makefile (c) 2023
# Desc: make commands for building
# Created:  2023-01-24T03:13:23.657Z
# Modified: !date!

MAKEFLAGS += --silent# suppress output

# # #
# config variables. you probably do not have to change them; they are dynamic
# # #
Docker := docker# docker binary. no spaces before comment!
ImageName := otterthings
ImageTag := build
TheImage := ${ImageName}:${ImageTag}
TheContainer := ${ImageName}-${ImageTag}
Pwd := $(shell pwd)


.PHONY: help build remove run stop rmc
.DEFAULT_GOAL = help

help:
	echo "Usage: make COMMAND"
	echo "Commands:"
	echo "	build		build Docker image as ${TheImage}"
	echo "	run [Pwd=PATH]	run Docker container from ${TheImage} with absolute PATH (default: cwd) mounted at /mnt"
	echo "	remove		remove the Docker image to free up drive space"
	echo "	stop		stops the running container ${TheContainer}"
	echo "	rmc			delete container ${TheContainer}"

build:
	echo "build image"
	DOCKER_BUILDKIT=1 ${Docker} build -t ${TheImage} .

remove:
	echo "remove the Docker image"
	-${Docker} image rm ${TheImage}

run:
	echo "run container from image ${Pwd}"
	${Docker} run --name ${TheContainer} -it -v "$$PWD:/mnt" ${TheImage} || make start

start:
	${Docker} start ${TheContainer} && docker attach ${TheContainer}

stop:
	-${Docker} stop ${TheContainer}

rmc: stop
	-${Docker} container rm ${TheContainer}
