FROM nginx:1.23.3-alpine

LABEL Author="Ottega (https://gitlab.com/ottega)"
LABEL Desc="Otterthings website"

RUN apk update && apk add bash

# copy website content into docker container
COPY public /usr/share/nginx/html

WORKDIR /usr/share/nginx/html